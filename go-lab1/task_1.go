package main

import "fmt"

func main() {
	numbers := []int{1, 2, 3, 4, 5, 1, 2, 3}
	solution := counter(numbers)
	fmt.Println(solution)
}
func counter(numbers []int) map[int]int {
	sol := make(map[int]int)

	for _, elem := range numbers {
		if _, ok := sol[elem]; ok {
			sol[elem] += 1
		} else {
			sol[elem] = 1
		}
	}

	return sol
}
