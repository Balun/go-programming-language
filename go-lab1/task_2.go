package main

import "fmt"

func main() {
	numbers := [][]int{
		[]int{3, 10, 4, 2},
		[]int{9, 3, 8, 7},
		[]int{15, 14, 13, 12},
	}
	solution := sretanBroj(numbers)
	fmt.Println(solution)
}
func sretanBroj(matrica [][]int) int {
	for _, val := range matrica {
		var min, j int
		for i, e := range val {
			if i == 0 || e < min {
				min = e
				j = i
			}
		}

		var max int
		for i := 0; i < cap(matrica); i++ {
			if i == 0 || matrica[i][j] > max {
				max = matrica[i][j]
			}
		}

		if min == max {
			return min
		}
	}

	return -1
}
