package main

import "fmt"

type Shopping struct {
	Name     string
	Price    int
	Quantity int
}

func main() {
	shopList := []Shopping{
		Shopping{"kruh", 8, 2},
		Shopping{"mlijeko", 15, 1}}

	solution1, err := mostExpensive(shopList)
	fmt.Println(solution1) // mlijeko
	fmt.Println(err)       // nil

	solution2 := totalCost(shopList)
	fmt.Println(solution2) // 31
}
func mostExpensive(shopList []Shopping) (item []Shopping, err error) {
	var max int

	if shopList == nil {
		err = fmt.Errorf("no data.")
		return item, err
	}

	if cap(shopList) == 0 {
		return item, err
	}

	for i, elem := range shopList {
		if i == 0 || elem.Price > max {
			max = elem.Price
		}
	}

	for _, elem := range shopList {
		if elem.Price == max {
			item = append(item, elem)
		}
	}

	return item, err
}
func totalCost(shopList []Shopping) (total int) {
	if shopList == nil {
		return 0
	}

	for _, elem := range shopList {
		total += elem.Price * elem.Quantity
	}

	return total
}
